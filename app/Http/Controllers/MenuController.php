<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Rules\RestoCategoryValidation;
use App\Services\MenuService;


class MenuController extends Controller
{
    public function saveMenuItem(Request $request)
    {
        $postData = $this->validate($request, [
            'category' => 'required',
            'price' => 'required|numeric',
            'item' => 'required',
        ]);

        return $postData;
    }
}
